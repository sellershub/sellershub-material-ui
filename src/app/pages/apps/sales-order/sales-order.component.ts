import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger60ms } from 'src/@vex/animations/stagger.animation';
import icRefresh from '@iconify/icons-ic/refresh';
import icSearch from '@iconify/icons-ic/search';
import icDownload from '@iconify/icons-ic/cloud-download';
import icAdd from '@iconify/icons-ic/twotone-add';
import { TableColumn } from 'src/@vex/interfaces/table-column.interface';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { Observable, ReplaySubject } from 'rxjs';
import { UrlAPIService } from 'src/app/services/url-api.service';
import { filter } from 'rxjs/operators';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icMail from '@iconify/icons-ic/twotone-mail';
import icMap from '@iconify/icons-ic/twotone-map';
import { SalesOrders } from 'src/app/services/sales-orders';
import icExcel  from '@iconify/icons-ic/bookmark-border'
import icPrint from '@iconify/icons-ic/twotone-print';
import icArrowRight from '@iconify/icons-ic/sharp-arrow-right';
import icMoney from '@iconify/icons-ic/monetization-on'
import icProcess from '@iconify/icons-ic/all-inclusive';
import icTruck from '@iconify/icons-ic/track-changes';
import icRemove from '@iconify/icons-ic/delete';
import icList from '@iconify/icons-ic/list';
// import faExcel from '@iconify/icons-fa-brands/fa-file';
import icCube from '@iconify/icons-ic/account-box';
import { MatDialog } from '@angular/material/dialog';
import icChip from '@iconify/icons-fa-solid/microchip';
import icfaPrint from '@iconify/icons-fa-solid/print';
import icmoney from '@iconify/icons-fa-solid/money-bill';
import icGift from '@iconify/icons-fa-solid/gift';
// import { PopoverRef } from '../../../../components/popover/popover-ref';
import { Router } from '@angular/router';
import { DispatchconsoleModelComponent } from 'src/app/pages/apps/dispatchconsole-model/dispatchconsole-model.component';
import { CreateorderModalComponent } from 'src/app/pages/apps/createorder-modal/createorder-modal.component';
import { ChannelproductsModelComponent } from 'src/app/pages/apps/channelproducts-model/channelproducts-model.component';
import { OrdercommentsshowModelComponent } from 'src/app/pages/apps/ordercommentsshow-model/ordercommentsshow-model.component';
import { OrdercommentscreateModelComponent } from 'src/app/pages/apps/ordercommentscreate-model/ordercommentscreate-model.component';
import { PicklistModelComponent } from 'src/app/pages/apps/picklist-model/picklist-model.component';
import { PacklistModelComponent } from 'src/app/pages/apps/packlist-model/packlist-model.component';
import { ExportcsvModelComponent } from 'src/app/pages/apps/exportcsv-model/exportcsv-model.component';
import { SendmailModelComponent } from 'src/app/pages/apps/sendmail-model/sendmail-model.component';
import { DownloadorderModelComponent } from 'src/app/pages/apps/downloadorder-model/downloadorder-model.component';



@Component({
  selector: 'vex-sales-order',
  templateUrl: './sales-order.component.html',
  styleUrls: ['./sales-order.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    stagger60ms,
    fadeInUp400ms
  ]
})
export class SalesOrderComponent implements OnInit {

  icRefresh = icRefresh;
  icSearch = icSearch;
  icDownload = icDownload;
  icPhone = icPhone;
  icMail = icMail;
  icMap = icMap;
  icAdd = icAdd;
  icExcel = icExcel;
  icPrint = icPrint;
  icArrowRight = icArrowRight;
  icMoney = icMoney;
  icProcess = icProcess;
  icTruck = icTruck;
  icRemove = icRemove;
  icList = icList;
  icCube = icCube;
  icChip =icChip;
  icfaPrint = icfaPrint;
  icmoney = icmoney;
  icGift = icGift;

  subject$: ReplaySubject<SalesOrders[]> = new ReplaySubject<SalesOrders[]>(1);
  data$: Observable<SalesOrders[]> = this.subject$.asObservable();
  sales_orders: SalesOrders[];

  @Input()
  columns: TableColumn<SalesOrders>[] = [
    { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    { label: 'id', property: 'id', type: 'text', visible: false },
    { label: 'Order Date', property: 'order_date_new', type: 'button', visible: true },
    { label: 'order time', property: 'order_time', type: 'button', visible: false },
    { label: 'Order Id', property: 'order_id', type: 'button', visible: true },
    { label: 'order status', property: 'order_status', type: 'text', visible: false },
    { label: 'order img', property: 'order_img', type: 'text', visible: false },
    { label: 'order img country', property: 'order_img_country', type: 'text', visible: false },
    { label: 'order img mail', property: 'order_img_mail', type: 'text', visible: false },
    { label: 'sku', property: 'sku', type: 'text', visible: false },
    { label: 'item id', property: 'item_id', type: 'button', visible: false },
    { label: 'Product', property: 'product_name', type: 'button', visible: true },
    { label: 'Item Total', property: 'item_total', type: 'text', visible: false },
    { label: 'Qty', property: 'qty', type: 'button', visible: true },
    { label: 'Shipped Address', property: 'ship_name', type: 'button', visible: true },
    { label: 'ship email', property: 'ship_email', type: 'text', visible: false },
    { label: 'ship address', property: 'ship_address', type: 'text', visible: false },
    { label: 'ship code', property: 'ship_code', type: 'text', visible: false },
    { label: 'ship country', property: 'ship_country', type: 'text', visible: false },
    { label: 'notes', property: 'notes', type: 'button', visible: true },
    { label: "Action", property: "action", type: 'button', visible: true }
  ];

  
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];

  dataSource: MatTableDataSource<SalesOrders> | null;
  selection = new SelectionModel<SalesOrders>(true, []);


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private cd: ChangeDetectorRef, private urlApi: UrlAPIService, private dialog: MatDialog) { }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit(): void {
    this.urlApi.getSalesOrders("sales-order.json")
      .subscribe((sales_orders) => {
        this.subject$.next(sales_orders);
    });

    this.dataSource = new MatTableDataSource();
    
    this.data$.pipe(
      filter<SalesOrders[]>(Boolean)
    ).subscribe(sales_orders => {
      this.sales_orders = sales_orders;
      this.dataSource.data = sales_orders;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  openDispatchModal(){
    this.dialog.open(DispatchconsoleModelComponent, {
      width: '100%',
      maxWidth: 850
    })
  }

  openCreateorderModal(){
    this.dialog.open(CreateorderModalComponent, {
      width: '100%',
      maxWidth: 850
    })
  }

  openChannelproductModal(){
    this.dialog.open(ChannelproductsModelComponent, {
      width: '100%',
      maxWidth: 850
    })
  }

  openCommentShowModal(){
    this.dialog.open(OrdercommentsshowModelComponent, {
      width: '100%',
      maxWidth: 850
    })
  }

  openCommentCreateModal(){
    this.dialog.open(OrdercommentscreateModelComponent, {
      width: '100%',
      maxWidth: 600
    })
  }

  openPicklistModal(){
    this.dialog.open(PicklistModelComponent, {
      width: '100%',
      maxWidth: 300
    })
  }

  openPacklistModal(){
    this.dialog.open(PacklistModelComponent, {
      width: '100%',
      maxWidth: 300
    })
  }

  openExportCSVModal(){
    this.dialog.open(ExportcsvModelComponent, {
      width: '100%',
      maxWidth: 300
    })
  }

  openSendMailModal(){
    this.dialog.open(SendmailModelComponent, {
      width: '100%',
      maxWidth: 800
    })
  }

}
