import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger60ms } from 'src/@vex/animations/stagger.animation';
import icRefresh from '@iconify/icons-ic/refresh';
import icSearch from '@iconify/icons-ic/search';
import icDownload from '@iconify/icons-ic/cloud-download';
import { TableColumn } from 'src/@vex/interfaces/table-column.interface';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { Observable, ReplaySubject } from 'rxjs';
import { ReportOrder } from 'src/app/services/report-order';
import { UrlAPIService } from 'src/app/services/url-api.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'vex-report-order-history',
  templateUrl: './report-order-history.component.html',
  styleUrls: ['./report-order-history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    stagger60ms,
    fadeInUp400ms
  ]
})
export class ReportOrderHistoryComponent implements OnInit {

  icRefresh = icRefresh;
  icSearch = icSearch;
  icDownload = icDownload;

  subject$: ReplaySubject<ReportOrder[]> = new ReplaySubject<ReportOrder[]>(1);
  data$: Observable<ReportOrder[]> = this.subject$.asObservable();
  historyreportorder: ReportOrder[];

  reportData;
  reportDatas;
  reportStructure: ReportOrder[];
  @Input()
  columns: TableColumn<ReportOrder>[] = [
    { label: 'Date', property: 'date', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Sub Total', property: 'subtotal', type: 'text', visible: true },
    { label: 'S&H', property: 'sh', type: 'text', visible: true },
    { label: 'Tax', property: 'tax', type: 'text', visible: true },
    { label: 'Total', property: 'total', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Total %', property: 'total_persen', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Units', property: 'units', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Unit %', property: 'units_persen', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Price/Units', property: 'price', type: 'text', visible: true }
  ];

  
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];

  dataSource: MatTableDataSource<ReportOrder> | null;
  selection = new SelectionModel<ReportOrder>(true, []);


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private cd: ChangeDetectorRef, private urlApi: UrlAPIService) { }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  

  ngOnInit(): void {
    this.urlApi.getReportOrder("order-history.json")
      .subscribe((historyreportorder) => {
        this.subject$.next(historyreportorder);
    });

    this.dataSource = new MatTableDataSource();
    
    this.data$.pipe(
      filter<ReportOrder[]>(Boolean)
    ).subscribe(historyreportorder => {
      this.historyreportorder = historyreportorder;
      this.dataSource.data = historyreportorder;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}
