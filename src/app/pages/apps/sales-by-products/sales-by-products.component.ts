import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger60ms } from 'src/@vex/animations/stagger.animation';
import icRefresh from '@iconify/icons-ic/refresh';
import icSearch from '@iconify/icons-ic/search';
import icDownload from '@iconify/icons-ic/cloud-download';
import { TableColumn } from 'src/@vex/interfaces/table-column.interface';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { Observable, ReplaySubject } from 'rxjs';
import { ReportByProduct } from 'src/app/services/report-by-product';
import { UrlAPIService } from 'src/app/services/url-api.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'vex-sales-by-products',
  templateUrl: './sales-by-products.component.html',
  styleUrls: ['./sales-by-products.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    stagger60ms,
    fadeInUp400ms
  ]
})



export class SalesByProductsComponent implements OnInit {
  
  icRefresh = icRefresh;
  icSearch = icSearch;
  icDownload = icDownload;

  subject$: ReplaySubject<ReportByProduct[]> = new ReplaySubject<ReportByProduct[]>(1);
  data$: Observable<ReportByProduct[]> = this.subject$.asObservable();
  reportbyproduct: ReportByProduct[];

  reportData;
  reportDatas;
  reportStructure: ReportByProduct[];
  @Input()
  columns: TableColumn<ReportByProduct>[] = [
    { label: 'SKU', property: 'sku', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Product', property: 'product', type: 'text', visible: true },
    { label: 'Total', property: 'total', type: 'text', visible: true },
    { label: 'Total %', property: 'total_persen', type: 'text', visible: true },
    { label: 'Unit', property: 'units', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Unit %', property: 'units_persen', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Price/Units', property: 'price', type: 'text', visible: true }
  ];

  
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];

  dataSource: MatTableDataSource<ReportByProduct> | null;
  selection = new SelectionModel<ReportByProduct>(true, []);


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private cd: ChangeDetectorRef, private urlApi: UrlAPIService) { }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  

  ngOnInit(): void {
    this.urlApi.getReportByProduct("report-sales-by-product.json")
      .subscribe((reportbyproduct) => {
        this.subject$.next(reportbyproduct);
    });

    this.dataSource = new MatTableDataSource();
    
    this.data$.pipe(
      filter<ReportByProduct[]>(Boolean)
    ).subscribe(reportbyproduct => {
      this.reportbyproduct = reportbyproduct;
      this.dataSource.data = reportbyproduct;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}