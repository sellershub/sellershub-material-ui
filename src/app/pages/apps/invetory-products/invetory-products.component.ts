import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger60ms } from 'src/@vex/animations/stagger.animation';
import icRefresh from '@iconify/icons-ic/refresh';
import icSearch from '@iconify/icons-ic/search';
import icDownload from '@iconify/icons-ic/cloud-download';
import icAdd from '@iconify/icons-ic/twotone-add';
import { TableColumn } from 'src/@vex/interfaces/table-column.interface';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { Observable, ReplaySubject } from 'rxjs';
import { UrlAPIService } from 'src/app/services/url-api.service';
import { filter } from 'rxjs/operators';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icMail from '@iconify/icons-ic/twotone-mail';
import icMap from '@iconify/icons-ic/twotone-map';
import { Products } from 'src/app/services/product';
import icExcel  from '@iconify/icons-ic/bookmark-border'
import icPrint from '@iconify/icons-ic/twotone-print';
import icRemove from '@iconify/icons-ic/remove-shopping-cart';
import icEdit from '@iconify/icons-ic/edit';

import icArrowRight from '@iconify/icons-ic/sharp-arrow-right';
import icMoney from '@iconify/icons-ic/monetization-on'
import icProcess from '@iconify/icons-ic/all-inclusive';
import icTruck from '@iconify/icons-ic/track-changes';
import icList from '@iconify/icons-ic/list';
import icCube from '@iconify/icons-ic/account-box';

@Component({
  selector: 'vex-invetory-products',
  templateUrl: './invetory-products.component.html',
  styleUrls: ['./invetory-products.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    stagger60ms,
    fadeInUp400ms
  ]
})
export class InvetoryProductsComponent implements OnInit {

  icRefresh = icRefresh;
  icSearch = icSearch;
  icDownload = icDownload;
  icPhone = icPhone;
  icMail = icMail;
  icMap = icMap;
  icAdd = icAdd;
  icExcel = icExcel;
  icPrint = icPrint;
  icRemove = icRemove;
  icEdit = icEdit;
  icArrowRight = icArrowRight;
  icMoney = icMoney;
  icProcess = icProcess;
  icTruck = icTruck;
  icList = icList;
  icCube = icCube;

  subject$: ReplaySubject<Products[]> = new ReplaySubject<Products[]>(1);
  data$: Observable<Products[]> = this.subject$.asObservable();
  list_products: Products[];

  @Input()
  columns: TableColumn<Products>[] = [
    { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    { label: 'Image', property: 'image_data', type: 'button', visible: true },
    { label: 'SKU', property: 'sku', type: 'text', visible: true },
    { label: 'Variations', property: 'variations', type: 'text', visible: true },
    { label: 'Channel', property: 'channel', type: 'button', visible: true },
    { label: 'Qty', property: 'qty', type: 'text', visible: true },
    { label: 'Price', property: 'price', type: 'text', visible: true },
    { label: 'Last Update', property: 'last_update', type: 'text', visible: true },
    { label: 'Action', property: 'action', type: 'button', visible: true }
  ];

  
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];

  dataSource: MatTableDataSource<Products> | null;
  selection = new SelectionModel<Products>(true, []);


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private cd: ChangeDetectorRef, private urlApi: UrlAPIService) { }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit(): void {
    this.urlApi.getProducts("product.json")
      .subscribe((list_products) => {
        this.subject$.next(list_products);
    });

    this.dataSource = new MatTableDataSource();
    
    this.data$.pipe(
      filter<Products[]>(Boolean)
    ).subscribe(list_products => {
      this.list_products = list_products;
      this.dataSource.data = list_products;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

}
