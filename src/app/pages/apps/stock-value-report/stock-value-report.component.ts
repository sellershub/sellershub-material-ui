import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger60ms } from 'src/@vex/animations/stagger.animation';
import icRefresh from '@iconify/icons-ic/refresh';
import icSearch from '@iconify/icons-ic/search';
import icDownload from '@iconify/icons-ic/cloud-download';
import icAdd from '@iconify/icons-ic/twotone-add';
import { TableColumn } from 'src/@vex/interfaces/table-column.interface';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { Observable, ReplaySubject } from 'rxjs';
import { ReportStockValue } from 'src/app/services/report-stock-value';
import { UrlAPIService } from 'src/app/services/url-api.service';
import { filter } from 'rxjs/operators';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icMail from '@iconify/icons-ic/twotone-mail';
import icMap from '@iconify/icons-ic/twotone-map';

@Component({
  selector: 'vex-stock-value-report',
  templateUrl: './stock-value-report.component.html',
  styleUrls: ['./stock-value-report.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    stagger60ms,
    fadeInUp400ms
  ]
})
export class StockValueReportComponent implements OnInit {

  icRefresh = icRefresh;
  icSearch = icSearch;
  icDownload = icDownload;
  icPhone = icPhone;
  icMail = icMail;
  icMap = icMap;
  icAdd = icAdd;

  subject$: ReplaySubject<ReportStockValue[]> = new ReplaySubject<ReportStockValue[]>(1);
  data$: Observable<ReportStockValue[]> = this.subject$.asObservable();
  stockvreport: ReportStockValue[];

  @Input()
  columns: TableColumn<ReportStockValue>[] = [
    { label: 'SKU', property: 'sku', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Title', property: 'title', type: 'text', visible: true },
    { label: 'Available Qty', property: 'available_qty', type: 'text', visible: true },
    { label: 'ITEM COST', property: 'item_cost', type: 'button', visible: true },
    { label: 'Stock Value', property: 'stock_value', type: 'text', visible: true }
  ];

  
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];

  dataSource: MatTableDataSource<ReportStockValue> | null;
  selection = new SelectionModel<ReportStockValue>(true, []);


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private cd: ChangeDetectorRef, private urlApi: UrlAPIService) { }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  

  ngOnInit(): void {
    this.urlApi.getStockValueReport("stock-value-report.json")
      .subscribe((stockvreport) => {
        this.subject$.next(stockvreport);
    });

    this.dataSource = new MatTableDataSource();
    
    this.data$.pipe(
      filter<ReportStockValue[]>(Boolean)
    ).subscribe(stockvreport => {
      this.stockvreport = stockvreport;
      this.dataSource.data = stockvreport;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}
