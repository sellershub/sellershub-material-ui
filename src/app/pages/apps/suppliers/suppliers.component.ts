import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UrlAPIService } from 'src/app/services/url-api.service';
import icEdit from '@iconify/icons-fa-solid/pencil-alt';
import icRemove from '@iconify/icons-fa-solid/trash';
import icArrowRight from '@iconify/icons-ic/sharp-arrow-right';
import icCheck from '@iconify/icons-ic/check';
import { Managesupplier } from 'src/app/services/manage_supplier'; 
import { Supplierfeeds } from 'src/app/services/supplierfeed';
import { TableColumn } from 'src/@vex/interfaces/table-column.interface';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Observable, ReplaySubject } from 'rxjs';
import { filter } from 'rxjs/operators';
// import { }

@Component({
  selector: 'vex-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.scss']
})
export class SuppliersComponent implements OnInit {

  managesupplier: Managesupplier[];
  supplierfeeds: Supplierfeeds[];

  icEdit = icEdit;
  icRemove = icRemove;
  icArrowRight = icArrowRight;
  icCheck = icCheck;

  subject$: ReplaySubject<Managesupplier[]> = new ReplaySubject<Managesupplier[]>(1);
  data$: Observable<Managesupplier[]> = this.subject$.asObservable();
  man_supplier: Managesupplier[];

  subject_new$: ReplaySubject<Supplierfeeds[]> = new ReplaySubject<Supplierfeeds[]>(1);
  data_new$: Observable<Supplierfeeds[]> = this.subject_new$.asObservable();
  supplier_feed: Supplierfeeds[];

  columns: TableColumn<Managesupplier>[] = [
    // { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    { label: '#', property: 'id', type: 'text', visible: true },
    { label: 'Supplier Name', property: 'supplier_name', type: 'text', visible: true },
    { label: 'Action', property: 'action', type: 'button', visible: true },
  ];

  columns_new: TableColumn<Supplierfeeds>[] = [
    // { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    { label: 'Feed URL', property: 'feed_url', type: 'button', visible: true },
    { label: 'Feed Format', property: 'feed_format', type: 'text', visible: true },
    { label: 'Rules', property: 'rules', type: 'button', visible: true },
    { label: 'Status', property: 'status', type: 'text', visible: true },
    // { label: 'Feed URL', property: 'supplier_name', type: 'text', visible: true },
    { label: 'Action', property: 'action', type: 'button', visible: true },
  ];

  
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];

  dataSource: MatTableDataSource<Managesupplier> | null;
  selection = new SelectionModel<Managesupplier>(true, []);

  dataSource_1: MatTableDataSource<Supplierfeeds> | null;
  selection_1 = new SelectionModel<Supplierfeeds>(true, []);


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor( private urlApi: UrlAPIService, private router: Router ) { }
  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  get visibleColumns_new() {
    return this.columns_new.filter(column1 => column1.visible).map(column1 => column1.property);
  }
  ngOnInit(): void {
    this.getManagesupplier();
    this.getSupplierfeeds();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.dataSource_1.paginator = this.paginator;
    this.dataSource_1.sort = this.sort;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
    isAllSelected_new() {
    const numSelected_1 = this.selection_1.selected.length;
    const numRows_1 = this.dataSource_1.data.length;
    return numSelected_1 === numRows_1;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  masterToggle_new() {
    this.isAllSelected_new() ?
      this.selection_1.clear() :
      this.dataSource_1.data.forEach(row => this.selection_1.select(row));
  }

  getManagesupplier() {
    this.urlApi.getManagesupplier("managesupplier.json")
      .subscribe((man_supplier) => {
        this.subject$.next(man_supplier);
    });

    this.dataSource = new MatTableDataSource();
    
    this.data$.pipe(
      filter<Managesupplier[]>(Boolean)
    ).subscribe(man_supplier => {
      this.man_supplier = man_supplier;
      this.dataSource.data = man_supplier;
    });

    
  }

  getSupplierfeeds() {
    // this.urlApi.getSupplierfeeds("supplierfeeds.json")
    //     .subscribe((response) => {
    //       console.log(response);
    //       this.supplierfeeds = response;
    //     })

    this.urlApi.getSupplierfeeds("supplierfeeds.json")
      .subscribe((supplier_feed) => {
        this.subject_new$.next(supplier_feed);
    });

    this.dataSource_1 = new MatTableDataSource();
    
    this.data_new$.pipe(
      filter<Supplierfeeds[]>(Boolean)
    ).subscribe(supplier_feed => {
      this.supplier_feed = supplier_feed;
      this.dataSource_1.data = supplier_feed;
    });
  }
}
