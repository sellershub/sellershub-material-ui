import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger60ms } from 'src/@vex/animations/stagger.animation';
import icRefresh from '@iconify/icons-ic/refresh';
import icSearch from '@iconify/icons-ic/search';
import icDownload from '@iconify/icons-ic/cloud-download';
import icAdd from '@iconify/icons-ic/twotone-add';
import { TableColumn } from 'src/@vex/interfaces/table-column.interface';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { Observable, ReplaySubject } from 'rxjs';
import { UrlAPIService } from 'src/app/services/url-api.service';
import { filter } from 'rxjs/operators';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icMail from '@iconify/icons-ic/twotone-mail';
import icMap from '@iconify/icons-ic/twotone-map';
import icEdit from '@iconify/icons-fa-solid/pencil-alt';
import icRemove from '@iconify/icons-fa-solid/trash';
import icArrowRight from '@iconify/icons-ic/sharp-arrow-right';
import icCheck from '@iconify/icons-ic/check';
import { Settingunusedbarcode } from 'src/app/services/setting_unused_barcode'; 
import { Settingusedbarcode } from 'src/app/services/setting_used_barcode';  
import { Settingskubarcode } from 'src/app/services/setting_sku_barcode'; 
import icExcel  from '@iconify/icons-ic/bookmark-border'
import icPrint from '@iconify/icons-ic/twotone-print';
import icCloudupload from '@iconify/icons-ic/cloud-upload';
import { Router } from '@angular/router';

@Component({
  selector: 'vex-setting-barcode-management',
  templateUrl: './setting-barcode-management.component.html',
  styleUrls: ['./setting-barcode-management.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    stagger60ms,
    fadeInUp400ms
  ]
})
export class SettingBarcodeManagementComponent implements OnInit {

  // settingunusedbarcode: Settingunusedbarcode[];
  // settingusedbarcode: Settingusedbarcode[];

  icEdit = icEdit;
  icRemove = icRemove;
  icArrowRight = icArrowRight;
  icCheck = icCheck;
  icRefresh = icRefresh;
  icSearch = icSearch;
  icDownload = icDownload;
  icPhone = icPhone;
  icMail = icMail;
  icMap = icMap;
  icAdd = icAdd;
  icExcel = icExcel;
  icPrint = icPrint;
  icCloudupload = icCloudupload


  subject$: ReplaySubject<Settingunusedbarcode[]> = new ReplaySubject<Settingunusedbarcode[]>(1);
  data$: Observable<Settingunusedbarcode[]> = this.subject$.asObservable();
  settingunusedbarcode: Settingunusedbarcode[];

  subject_new$: ReplaySubject<Settingusedbarcode[]> = new ReplaySubject<Settingusedbarcode[]>(1);
  data_new$: Observable<Settingusedbarcode[]> = this.subject_new$.asObservable();
  settingusedbarcode: Settingusedbarcode[];

  subject_2$: ReplaySubject<Settingskubarcode[]> = new ReplaySubject<Settingskubarcode[]>(1);
  data_2$: Observable<Settingskubarcode[]> = this.subject_2$.asObservable();
  settingskubarcode: Settingskubarcode[];

  columns: TableColumn<Settingunusedbarcode>[] = [
    // { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    // { label: 'Image', property: 'image_data', type: 'button', visible: true },
    { label: 'UPC', property: 'upc', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'EAN', property: 'ean', type: 'text', visible: true, cssClasses: ['font-medium'] },
    // { label: 'Status', property: 'status', type: 'text', visible: true, cssClasses: ['font-medium'] },
    // { label: 'Download', property: 'download', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Action', property: 'action', type: 'button', visible: true },
  ];

  columns_new: TableColumn<Settingusedbarcode>[] = [
    // { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    { label: 'Assigned SKU', property: 'assigned_sku', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'UPC ', property: 'upc', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'EAN', property: 'ean', type: 'text', visible: true, cssClasses: ['font-medium'] },
    // { label: 'Download', property: 'download', type: 'text', visible: true, cssClasses: ['font-medium'] },
    // { label: 'Action', property: 'action', type: 'button', visible: true },
  ];

  columns_2: TableColumn<Settingskubarcode>[] = [
    // { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    { label: 'SKU', property: '_sku', type: 'text', visible: true, cssClasses: ['font-medium'] },
    // { label: 'UPC ', property: 'upc', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Barcode', property: 'barcode', type: 'button', visible: true},
    // { label: 'Download', property: 'download', type: 'text', visible: true, cssClasses: ['font-medium'] },
    // { label: 'Action', property: 'action', type: 'button', visible: true },
  ];

  
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];

  dataSource: MatTableDataSource<Settingunusedbarcode> | null;
  selection = new SelectionModel<Settingunusedbarcode>(true, []);

  dataSource_1: MatTableDataSource<Settingusedbarcode> | null;
  selection_1 = new SelectionModel<Settingusedbarcode>(true, []);

  dataSource_2: MatTableDataSource<Settingskubarcode> | null;
  selection_2 = new SelectionModel<Settingskubarcode>(true, []);


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor( private urlApi: UrlAPIService, private router: Router ) { }
  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  get visibleColumns_new() {
    return this.columns_new.filter(column1 => column1.visible).map(column1 => column1.property);
  }

  get visibleColumns_2() {
    return this.columns_2.filter(column2 => column2.visible).map(column2 => column2.property);
  }

  ngOnInit(): void {
    this.getSettingunusedbarcode();
    this.getSettingusedbarcode();
    this.getSettingskubarcode();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.dataSource_1.paginator = this.paginator;
    this.dataSource_1.sort = this.sort;

    this.dataSource_2.paginator = this.paginator;
    this.dataSource_2.sort = this.sort;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
    isAllSelected_new() {
    const numSelected_1 = this.selection_1.selected.length;
    const numRows_1 = this.dataSource_1.data.length;
    return numSelected_1 === numRows_1;
  }
  isAllSelected_2() {
  const numSelected_2 = this.selection_2.selected.length;
  const numRows_2 = this.dataSource_2.data.length;
  return numSelected_2 === numRows_2;
}

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  masterToggle_new() {
    this.isAllSelected_new() ?
      this.selection_1.clear() :
      this.dataSource_1.data.forEach(row => this.selection_1.select(row));
  }

  masterToggle_2() {
    this.isAllSelected_new() ?
      this.selection_2.clear() :
      this.dataSource_2.data.forEach(row => this.selection_2.select(row));
  }

  getSettingunusedbarcode() {
    this.urlApi.getSettingunusedbarcode("setting_unused_barcode.json")
    .subscribe((settingunusedbarcode) => {
      this.subject$.next(settingunusedbarcode);
  });

  this.dataSource = new MatTableDataSource();
  
  this.data$.pipe(
    filter<Settingunusedbarcode[]>(Boolean)
  ).subscribe(settingunusedbarcode => {
    this.settingunusedbarcode = settingunusedbarcode;
    this.dataSource.data = settingunusedbarcode;
  });

    
  }

  getSettingusedbarcode() {
    // this.urlApi.getSupplierfeeds("supplierfeeds.json")
    //     .subscribe((response) => {
    //       console.log(response);
    //       this.supplierfeeds = response;
    //     })

    this.urlApi.getSettingusedbarcode("setting_used_barcode.json")
      .subscribe((settingusedbarcode) => {
        this.subject_new$.next(settingusedbarcode);
    });

    this.dataSource_1 = new MatTableDataSource();
    
    this.data_new$.pipe(
      filter<Settingusedbarcode[]>(Boolean)
    ).subscribe(settingusedbarcode => {
      this.settingusedbarcode = settingusedbarcode;
      this.dataSource_1.data = settingusedbarcode;
    });
  }

  getSettingskubarcode() {
    

    this.urlApi.getSettingskubarcode("setting_used_barcode.json")
      .subscribe((settingskubarcode) => {
        this.subject_2$.next(settingskubarcode);
    });

    this.dataSource_2 = new MatTableDataSource();
    
    this.data_2$.pipe(
      filter<Settingskubarcode[]>(Boolean)
    ).subscribe(settingskubarcode => {
      this.settingskubarcode = settingskubarcode;
      this.dataSource_2.data = settingskubarcode;
    });
  }
}
