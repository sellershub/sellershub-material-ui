import { Component, OnInit } from '@angular/core';
import icMail from '@iconify/icons-ic/mail-outline';
import { MatDialog } from '@angular/material/dialog';
import { EmaildialogueModelComponent } from 'src/app/pages/apps/emaildialogue-model/emaildialogue-model.component';

@Component({
  selector: 'vex-setting-email',
  templateUrl: './setting-email.component.html',
  styleUrls: ['./setting-email.component.scss']
})


export class SettingEmailComponent implements OnInit {
  inputType = 'password';
  icMail = icMail;
  constructor( private dialog: MatDialog ) { }

  ngOnInit(): void {
  }

  openEmaildialogeModel(){
    this.dialog.open(EmaildialogueModelComponent, { 
      width: '100%',
      maxWidth: 850
     })
  }

}
