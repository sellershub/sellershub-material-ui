export class DashboardLowStock {
    id: number;
    sku: string;
    product: string;
    stock: number;
    level: number;
  
    constructor(lowstock) {
      this.id = lowstock.id;
      this.sku = lowstock.sku;
      this.product = lowstock.product;
      this.stock = lowstock.stock;
      this.level = lowstock.level;
    }
}
  