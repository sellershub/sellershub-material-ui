import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { ChannelIntegration } from './channel-integration';
import { CountryList } from './country-list';
import { DashboardLowStock } from '../pages/dashboards/dashboard-analytics/dashboard.lowstock';
import { RecentOrders } from '../pages/dashboards/dashboard-analytics/dashboard.recentorders';
import { ReportByProduct } from './report-by-product';
import { LowStock } from './low-stock';
import { ReportStockValue } from './report-stock-value';
import { ReportOrder } from './report-order';
import { OrderSummary } from './order-summary';
import { McfOrder } from './mcf-orders';
import { ReturnedOrders } from './returned-orders';
import { CancelledOrders } from './cancelled-orders';
import { ShippedOrders } from './shipped-orders';
import { SalesOrders } from './sales-orders';
import { Products } from './product';
import { Waitingtolist } from './waitingtolist';
import { Listingerror } from './listingerror';
import { Listedproduct } from './listedproduct';
import { Closedproduct } from './closedproduct';
import { Schedulelisting } from './schedulelisting';
import { Fabcreateproduct } from './fab-createproducts';
import { Fabsendtofba } from './fabsendtofba';
import { Fabproductinfba } from './fab-productinfba';

import { Warehouse } from './warehouse';
import { Stocksummary } from './stocksummary';
import { Transfer } from './transfer';
import { Customer } from './customer';
import { Purchaseorder } from './purchaseorder';
import { Managesupplier } from './manage_supplier';
import { Supplierfeeds } from './supplierfeed';
import { Channelproducts } from './channelroducts';
import { Settingtax } from './setting_tax';
import { Settingorders } from './settingorders'; 
import { Settinginvoice } from './setting-invoice';
import { Settingattribute } from './setting_attributes';
import { Settingset } from './setting_set';
import { Settingbulkimport } from './setting_bulkimport';
import { Settingbulkexport } from './setting_bulkexport';
import { Settingnotification } from './setting_notification';
import { Settingunusedbarcode } from './setting_unused_barcode';
import { Settingusedbarcode } from './setting_used_barcode';
import { Settingskubarcode } from './setting_sku_barcode';
import { Settingsyncchanel } from './setting_sync_channel';
import { Settingexcludesync } from './setting_exclude_sync';

import { ChannelRegistration } from './channel-registration';
import { ChannelEcommerce } from './channel-ecommerce';
import { ChannelMarketPlace } from './channel-masrketplace';
import { ChannelAccounting } from './channel-accounting';
import { ChannelCrm } from './channel-crm';
import { ChannelEpos } from './channel-epos';
import { ChannelAppstore } from './channel-appstore';
import { ChannelOther } from './channel-other';
import { ShippingCourier } from './shipping-courier';
import { UsersData } from './users-data';
import { SubscribtionDetails } from './subscribtions-details';
import { InvoicesData } from './invoices-data';
import { MessageCenter } from './message-center';
import { TemplateDesigner } from './template-designer';
import { UserAuth } from './user-auth';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

import { Createorder } from './createorder';
import { Channelproductmodel } from './channelproduct-modal';
import { Sendmailmodel } from './sendmail-model';
import { Downloadordermodel } from './downloadorder-model';

@Injectable({
  providedIn: 'root'
})
export class UrlAPIService {

  BASE_URL: string = "http://54.193.80.200:8080";
  BASE_URL_DUMMY: string = "assets/json-data/";
  URL = "https://gist.githubusercontent.com/keeguon/2310008/raw/bdc2ce1c1e3f28f9cab5b4393c7549f38361be4e/countries.json";
  userRespon;
  
  constructor(private http: HttpClient, private snackbar: MatSnackBar, private router: Router) {}
  
  userAuth(userId: string, userPass: string){
    
    let headers = new HttpHeaders();
        headers.set('Content-Type', 'application/json');
        headers.set('Access-Control-Allow-Origin', '*');
        headers.set('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
        headers.set('Access-Control-Allow-Headers', 'Content-Type');

    const body = { userId: userId, password: userPass };
    return this.http.post(this.BASE_URL + '/login', body,{ headers: headers })
      .pipe(
        catchError((err) => {
          let errMsg = err.statusText;
          if(errMsg == "Unauthorized"){
            this.snackbar.open('Your email or password incorrect', 'Try Again', {
              duration: 10000
            });
          }
          return throwError(err);
        })
      )
      .subscribe(data => {
        this.userRespon = data;
        console.log(this.userRespon);
        this.snackbar.open('Successfull login, redirect to dashboard', 'Ok', {
          duration: 1000
        });
        localStorage.setItem('userLoginName', userId);
        localStorage.setItem('userLoginEmail', userPass);
        localStorage.setItem('userToken', this.userRespon.token);
        localStorage.setItem('userSubscribe', this.userRespon.subscriptionStatus);
        this.router.navigate(['/']);
      });

    }  
  getChannelIntegrations(params){
    return this.http.get<ChannelIntegration[]>(this.BASE_URL_DUMMY + params);
  }

  getListCountry(){
    return this.http.get<CountryList[]>(this.BASE_URL_DUMMY + "country-list.json");
  }

  getListRecentOrders(params) {
    return this.http.get<RecentOrders[]>(this.BASE_URL_DUMMY + params);
  }

  getListLowStock(params) {
    return this.http.get<DashboardLowStock[]>(this.BASE_URL_DUMMY + params);
  }
  
  getReportByProduct(params) {
    return this.http.get<ReportByProduct[]>(this.BASE_URL_DUMMY + params);
  }

  getReportlowStock(params){
    return this.http.get<LowStock[]>(this.BASE_URL_DUMMY + params);
  }

  getStockValueReport(params){
    return this.http.get<ReportStockValue[]>(this.BASE_URL_DUMMY + params);
  }

  getReportOrder(params){
    return this.http.get<ReportOrder[]>(this.BASE_URL_DUMMY + params);
  }

  getOrderSummary(paramms){
    return this.http.get<OrderSummary[]>(this.BASE_URL_DUMMY + paramms);
  }

  getMcfOrders(params){
    return this.http.get<McfOrder[]>(this.BASE_URL_DUMMY + params);
  }

  getReturnedOrders(params){
    return this.http.get<ReturnedOrders[]>(this.BASE_URL_DUMMY + params);
  }

  getCancelledOrders(params) {
    return this.http.get<CancelledOrders[]>(this.BASE_URL_DUMMY + params);
  }

  getShippedOrders(params) {
    return this.http.get<ShippedOrders[]>(this.BASE_URL_DUMMY + params);
  }

  getSalesOrders(params) {
    return this.http.get<SalesOrders[]>(this.BASE_URL_DUMMY + params);
  }

  getProducts(params) {
    return this.http.get<Products[]>(this.BASE_URL_DUMMY + params);
  }

  getWaitingtolist(params) {
    return this.http.get<Waitingtolist[]>(this.BASE_URL_DUMMY + params);
  }

  getListingerror(params) {
    return this.http.get<Listingerror[]>(this.BASE_URL_DUMMY + params);
  }

  getListedproduct(params) {
    return this.http.get<Listedproduct[]>(this.BASE_URL_DUMMY + params);
  }

  getClosedproduct(params) {
    return this.http.get<Closedproduct[]>(this.BASE_URL_DUMMY + params);
  }

  getSchedulelisting(params) {
    return this.http.get<Schedulelisting[]>(this.BASE_URL_DUMMY + params);
  }

  getFabcreateproduct(params) {
    return this.http.get<Fabcreateproduct[]>(this.BASE_URL_DUMMY + params);
  }

  getFabsendtofba(params) {
    return this.http.get<Fabsendtofba[]>(this.BASE_URL_DUMMY + params);
  }

  getFabproductinfba(params) {
    return this.http.get<Fabproductinfba[]>(this.BASE_URL_DUMMY + params);
  }



  getWarehouse(params) {
    return this.http.get<Warehouse[]>(this.BASE_URL_DUMMY + params);
  }

  getStocksummary(params) {
    return this.http.get<Stocksummary[]>(this.BASE_URL_DUMMY + params);
  }

  getTransfer(params) {
    return this.http.get<Transfer[]>(this.BASE_URL_DUMMY + params);
  }
  
  getCustomer(params) {
    return this.http.get<Customer[]>(this.BASE_URL_DUMMY + params);
  }
  
  getPurchaseorder(params) {
    return this.http.get<Purchaseorder[]>(this.BASE_URL_DUMMY + params);
  }
  
  getManagesupplier(params) {
    return this.http.get<Managesupplier[]>(this.BASE_URL_DUMMY + params);
  }
  
  getSupplierfeeds(params) {
    return this.http.get<Supplierfeeds[]>(this.BASE_URL_DUMMY + params);
  }
  
  getChannelproducts(params) {
    return this.http.get<Channelproducts[]>(this.BASE_URL_DUMMY + params);
  }
  
  getSettingtax(params) {
    return this.http.get<Settingtax[]>(this.BASE_URL_DUMMY + params);
  }
  
  getSettingorders(params) {
    return this.http.get<Settingorders[]>(this.BASE_URL_DUMMY + params);
  }
  
  getSettinginvoice(params) {
     return this.http.get<Settinginvoice[]>(this.BASE_URL_DUMMY + params);
   }
  
  getSettingattribute(params) {
    return this.http.get<Settingattribute[]>(this.BASE_URL_DUMMY + params);
  }
  
  getSettingset(params) {
    return this.http.get<Settingset[]>(this.BASE_URL_DUMMY + params);
  }
  
  getSettingbulkimport(params) {
    return this.http.get<Settingbulkimport[]>(this.BASE_URL_DUMMY + params);
  }
  
  getSettingbulkexport(params) {
    return this.http.get<Settingbulkexport[]>(this.BASE_URL_DUMMY + params);
  }
  
  getSettingnotification(params) {
    return this.http.get<Settingnotification[]>(this.BASE_URL_DUMMY + params);
  }
  
  getSettingunusedbarcode(params) {
    return this.http.get<Settingunusedbarcode[]>(this.BASE_URL_DUMMY + params);
  }
  
  getSettingusedbarcode(params) {
    return this.http.get<Settingusedbarcode[]>(this.BASE_URL_DUMMY + params);
  }
  
  getSettingskubarcode(params) {
    return this.http.get<Settingskubarcode[]>(this.BASE_URL_DUMMY + params);
  }
  
  getSettingsyncchannel(params) {
    return this.http.get<Settingsyncchanel[]>(this.BASE_URL_DUMMY + params);
  }
  
  getSettingexcludesync(params) {
    return this.http.get<Settingexcludesync[]>(this.BASE_URL_DUMMY + params);
  }
 


  getChannelMarketPlace(params){
    return this.http.get<ChannelMarketPlace[]>(this.BASE_URL_DUMMY + params);
  }

  getChannelRegistration(params){
    return this.http.get<ChannelRegistration[]>(this.BASE_URL_DUMMY + params);
  }

  getChannelEcommerce(paramms){
    return this.http.get<ChannelEcommerce[]>(this.BASE_URL_DUMMY + paramms);
  }

  getChannelAccounting(paramms){
    return this.http.get<ChannelAccounting[]>(this.BASE_URL_DUMMY + paramms);
  }

  getChannelCrm(params){
    return this.http.get<ChannelCrm[]>(this.BASE_URL_DUMMY + params);
  }

  getChannelEpos(params){
    return this.http.get<ChannelEpos[]>(this.BASE_URL_DUMMY + params);
  }

  getChannelAppstore(params){
    return this.http.get<ChannelAppstore[]>(this.BASE_URL_DUMMY + params);
  }

  getChannelOther(params){
    return this.http.get<ChannelOther[]>(this.BASE_URL_DUMMY + params);
  }

  getShippingCourier(params) {
    return this.http.get<ShippingCourier[]>(this.BASE_URL_DUMMY + params);
  }

  getUsersData() {
    
    let headers = new HttpHeaders();
        headers = headers.set('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJrYXJ0emh1Yl9zdWJzY3JpcHRpb25fc3RhdHVzIjoiQUNUSVZFIiwidXNlcl9lbWFpbCI6IjIzNThAa2FydHpodWIuY29tIiwiZXhwaXJlc19hdCI6MTYzMjY2ODk1MDk3MCwidXNlcl9mdWxsX25hbWUiOiJrYXJ0emh1YiBhZ2VudCIsImthcnR6aHViX3VzZXJfaWQiOjIzNTh9.cROGEbSzuvjyuwJ_Y_h1uEncEnd8s-hEq_vuchIk_qo');
        headers = headers.set('Content-Type', 'application/json');
        headers = headers.set('Access-Control-Allow-Origin', 'http://localhost:4200');
        headers = headers.set('Access-Control-Allow-Origin', '*');
        headers = headers.set('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
        headers = headers.set('Access-Control-Allow-Headers', 'Content-Type');
        console.log("Authorization: " + headers.getAll('Access-Control-Allow-Origin'));
    return this.http.get<UsersData[]>(this.BASE_URL + "/user", { headers: headers } );

  }

  getSubscriptionData(params) {
    return this.http.get<SubscribtionDetails[]>(this.BASE_URL_DUMMY + params);
  }

  getInvoice(params) {
    return this.http.get<InvoicesData[]>(this.BASE_URL_DUMMY + params);
  }

  getMessageCenter(params){
    return this.http.get<MessageCenter[]>(this.BASE_URL_DUMMY + params);
  }

  getTemplateDesigner(params){
    return this.http.get<TemplateDesigner[]>(this.BASE_URL_DUMMY + params);
  }


  getCreateorder(params){
    return this.http.get<Createorder[]>(this.BASE_URL_DUMMY + params);
  }

  getChannelproductmodel(params){
    return this.http.get<Channelproductmodel[]>(this.BASE_URL_DUMMY + params);
  }

  getSendmailmodel(params){
    return this.http.get<Sendmailmodel[]>(this.BASE_URL_DUMMY + params);
  }

  getDownloadordermodel(params){
    return this.http.get<Downloadordermodel[]>(this.BASE_URL_DUMMY + params);
  }
}