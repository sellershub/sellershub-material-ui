export class ReportStockValue {
    sku: string;
    title: string;
    available_qty: number;
    item_cost: string;
  }