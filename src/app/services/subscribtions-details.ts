export class SubscribtionDetails {
    date: string;
    plan: string;
    status: string;
    payment_mode: string;
    expired_on: string;
}