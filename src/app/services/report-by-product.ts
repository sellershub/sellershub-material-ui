export class ReportByProduct {
    sku: string;
    product: string;
    total: number;
    total_persen: number;
    units: string;
    units_persen: string;
    price: string;
  }