export class SalesOrders {
    id: string;
    order_date: string;
    item_id: string;
    product_name: string;
    qty: string;
    ship_name: string;
    notes: string;
}