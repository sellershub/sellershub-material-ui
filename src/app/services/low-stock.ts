export class LowStock {
    id: number;
    sku: string;
    product: string;
    available_stock: number;
    low_alert_level: number;
  }