export class Products {
    image: string;
    sku: string;
    variations: string;
    channel: string;
    qty: string;
    price: string;
    last_update: string;
}